const users = require('./data')

function residentOfGermany(users , country) {
    if(country.length !== 0) {
        let germany_residents = [];
        for(let user in users) {
            if(users[user].nationality !== undefined && users[user].nationality === country) {
                germany_residents.push(user);
            }
        }
        return germany_residents;
    } else {
        return [];
    }
}

const results = residentOfGermany(users , "Germany");

console.log(results);