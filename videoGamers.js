const users = require('./data')

function interest_video_game(users_object , query_interest) {
    if(query_interest.length !== 0) {
        let video_game_interest = [];
        for(const user in users_object) {
            if(users_object[user].interests !== undefined) {
                for(let each_interest of users_object[user].interests) {
                    if(each_interest.includes(query_interest)) {
                        video_game_interest.push(user);
                        break;
                    }
                }
            } else {
                for(let each_interest of users_object[user].interest) {
                    if(each_interest.includes(query_interest)) {
                        video_game_interest.push(user);
                        break;
                    }
                }
            }
        }
        return video_game_interest;
    } else {
        return [];
    }
}
const results = interest_video_game(users , "Video Games");

console.log(results);