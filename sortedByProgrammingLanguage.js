const users = require("./data");

function groupByProgrammingLanguage(users) {
  const all_programming_languages = [
    "Java",
    "Python",
    "Javascript",
    "Golang",
    "C",
    "C++",
    "C#",
  ];

  const result_sorting_by_programming_langauage = {};

  for (let user in users) {
    let present_user_desgination = users[user].desgination.split(" ");
    for (let each_word of present_user_desgination) {
      for (let language of all_programming_languages) {
        if (each_word === language) {
          if (result_sorting_by_programming_langauage[language] !== undefined) {
            result_sorting_by_programming_langauage[language].push(user);
          } else {
            result_sorting_by_programming_langauage[language] = [user];
          }
          break;
        }
      }
    }
  }
  return result_sorting_by_programming_langauage;
}

const results = groupByProgrammingLanguage(users);

console.log(results);
