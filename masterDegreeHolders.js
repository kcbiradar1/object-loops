const users = require('./data')

function masterDegree(users , degree_name) {
    if(degree_name.length !== 0) {
        let master_degree_holders = [];
        for(let user in users) {
            if(users[user].qualification !== undefined && users[user].qualification === degree_name) {
                master_degree_holders.push(user);
            }
        }
        return master_degree_holders;
    } else {
        return [];
    }
}

const results = masterDegree(users , "Masters");

console.log(results);